﻿//Date: 23 Oct
//Author: Cecily
//Purpose: The core algorithm of this application
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MontyHallCore : MonoBehaviour {

	//The array to store the information of the doors.
	public static string[] doors = new string[3];
	public static int selectedDoor;
	public static MontyHallCore montyHall;

	void start()
	{
		montyHall = this;
	}

	public static void PutCar()
	{
		//put goats behind every door.
		for (int i = 0; i < 3; i++) 
		{
			doors [i] = "goat";
		}

		//Randomly put the car behind one of the doors.
		doors [Random.Range(0,3)] = "car";
	}

	public static string SelectFirstDoor(int id)
	{
		selectedDoor = id;

		if (doors [selectedDoor].Equals ("goat")) 
		{
			return "goat";

		}else
		{
			return "car";
		}
	}

	public static bool IsKeepWin()
	{
		if (doors [selectedDoor].Equals ("goat")) 
		{
			//lose
			return false;
		}else{
			//win
			return true;
		}
	}

	public static bool IsSwitchWin()
	{
		if (doors [selectedDoor].Equals ("goat")) 
		{
			//win
			return true;
		}else{
			//lose
			return false;
		}
	}
}
