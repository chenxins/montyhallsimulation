﻿//Date: 23 Oct
//Author: Cecily
//Purpose: Manage results
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultManager : MonoBehaviour {

	public Text switchWin;
	public Text keepWin;
	public Text switchTotal;
	public Text keepTotal;

	public int switchWinCount=0;
	public int keepWinCount=0;
	public int switchTotalCount=0;
	public int keepTotalCount=0;


	public void SwitchWin()
	{
		switchWinCount++;
		switchTotalCount++;
		//update the UI text.
		switchWin.text = switchWinCount.ToString();
		switchTotal.text = switchTotalCount.ToString ();
	}

	public void SwitchLose()
	{
		switchTotalCount++;
		//update the UI text.
		switchTotal.text = switchTotalCount.ToString ();
	}

	public void KeepWin()
	{
		keepWinCount++;
		keepTotalCount++;
		//update the UI text.
		keepWin.text = keepWinCount.ToString();
		keepTotal.text = keepTotalCount.ToString ();
	}

	public void KeepLose()
	{
		keepTotalCount++;
		//update the UI text.
		keepTotal.text = keepTotalCount.ToString ();
	}

	public void Clear()
	{
		//Clear the results.
		switchWinCount = 0;
		keepWinCount = 0;
		switchTotalCount = 0;
		keepTotalCount = 0;
		//update the UI text.
		switchWin.text = switchWinCount.ToString();
		switchTotal.text = switchTotalCount.ToString ();
		keepWin.text = keepWinCount.ToString();
		keepTotal.text = keepTotalCount.ToString ();
	}
}
