﻿//Date: 23 Oct
//Author: Cecily
//Purpose: Manage the Game Mechanism
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	//The buttons that represent the doors.
	public Button door1;
	public Button door2;
	public Button door3;
	//The input field where the user input the simulation times.
	public InputField times;
	public GameObject resultManager;
	//The text that shows the result.
	public Text winText;


	List<Button> buttons = new List<Button>();

	void Start () 
	{
		MontyHallCore.PutCar();
		buttons.Add (door1);
		buttons.Add (door2);
		buttons.Add (door3);

	}

	public void FirstChoice(int id)
	{
		//Once the first door is chosen, disable all other doors
		door1.enabled=false;
		door2.enabled=false;
		door3.enabled=false;

		//Change the text on the selected doors to "Selected".
		buttons[id].GetComponentInChildren<Text>().text="Selected";

		if (MontyHallCore.SelectFirstDoor(id).Equals("goat")) 
		{
			//open the other goat door
			for (int i = 0; i < 3; i++) 
			{
				if (i != id && MontyHallCore.doors[i].Equals("goat")) {
					buttons[i].GetComponentInChildren<Text>().text="goat";
					break;
				}
			}

		}else
		{
			//randomly open one of the rest two doors
			for (int i = 0; i < 3; i++) 
			{
				if (i != id) 
				{
					buttons[i].GetComponentInChildren<Text>().text="goat";
					break;
				}
			}
		}
	}
	
	public void SwitchDoor()
	{
		if (MontyHallCore.IsSwitchWin()) 
		{
			//show win text
			winText.text="You Win!";
			resultManager.GetComponent<ResultManager> ().SwitchWin ();
		}else{
			//lose
			winText.text="You Lose!";
			resultManager.GetComponent<ResultManager> ().SwitchLose ();
		}

		//open all the doors
		for (int i = 0; i < 3; i++) 
		{
			buttons[i].GetComponentInChildren<Text>().text= MontyHallCore.doors[i];
		}
	}

	public void KeepDoor()
	{
		if (MontyHallCore.IsKeepWin()) 
		{
			//lose
			winText.text="You win!";
			resultManager.GetComponent<ResultManager> ().KeepWin ();
		}else{
			//win
			winText.text="You Lose!";
			resultManager.GetComponent<ResultManager> ().KeepLose ();
		}

		//open all the doors
		for (int i = 0; i < 3; i++) 
		{
			buttons[i].GetComponentInChildren<Text>().text= MontyHallCore.doors[i];
		}
	}

	public void Restart()
	{
		for (int i = 0; i < 3; i++) 
		{
			buttons [i].enabled = true;
			buttons [i].GetComponentInChildren<Text> ().text = (i+1).ToString ();
		}

		winText.text = "";
		MontyHallCore.PutCar ();
	}
		
	public void SimulateKeep()
	{
		if (!times.text.Equals ("")) 
		{
			for (int i = 0; i < int.Parse(times.text); i++) 
			{
				MontyHallCore.PutCar ();
				MontyHallCore.SelectFirstDoor (Random.Range(0,3));
				if (MontyHallCore.IsKeepWin()) {
					resultManager.GetComponent<ResultManager> ().KeepWin ();
				} else {
					resultManager.GetComponent<ResultManager> ().KeepLose ();

				}
			}
		}
	}

	public void SimulateSwitch()
	{
		if (!times.text.Equals ("")) 
		{
			for (int i = 0; i < int.Parse(times.text); i++) 
			{
				MontyHallCore.PutCar ();

				MontyHallCore.SelectFirstDoor (Random.Range(0,3));
				if (MontyHallCore.IsSwitchWin()) {
					resultManager.GetComponent<ResultManager> ().SwitchWin ();
				} else {
					resultManager.GetComponent<ResultManager> ().SwitchLose ();

				}
			}
		}
	}

	public void ClearResult()
	{
		resultManager.GetComponent<ResultManager> ().Clear (); 
	}
}
